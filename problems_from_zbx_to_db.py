from pyzabbix.api import ZabbixAPI
from datetime import datetime, timedelta
import pandas as pd
import numpy as np
from sqlalchemy import create_engine, types
import sys

def create_problems_df(df):
    def make_row(objectid, dict_, clock, r_clock, ack, message, priority, region):
        if clock and r_clock:
            if objectid in dict_.keys():
                dict_[objectid]['r_clock'] = r_clock
                dict_[objectid]['acknowledged'] += str(ack)
                dict_[objectid]['duration'] += int(r_clock)-int(clock)
                dict_[objectid]['count'] += 1
                if message is not None and message not in dict_[objectid]['message']:
                    dict_[objectid]['message'] += ' | ' + message

            else:
                dict_[objectid] = {}
                dict_[objectid]['clock'] = clock
                dict_[objectid]['r_clock'] = r_clock
                dict_[objectid]['acknowledged'] = str(ack)
                dict_[objectid]['duration'] = int(r_clock)-int(clock)
                dict_[objectid]['count'] = 1
                dict_[objectid]['priority'] = priority
                dict_[objectid]['region'] = region
                if message is not None:
                    dict_[objectid]['message'] = message
                else:
                    dict_[objectid]['message'] = ''




    corrupted_objectids = set()
    objectids_problems = {}
    none_objectids_problems = []
    for i in range(len(df)):
        print(i)
        dataframe = df[1][i]
        dataframe.sort_values(by=['clock'], inplace=True)
        objectid = dataframe['objectid'].iloc[0]
        count =  min(dataframe[dataframe['value'] == '0']['value'].count(), dataframe[dataframe['value'] == '1']['value'].count())
        df_len = len(dataframe)
        offset = 0
        clock=None
        ack=None
        r_clock = None
        for k in range(df_len):
            if k + offset <= df_len - 1:
                if dataframe['value'].iloc[k+offset] == '1':
                    clock = dataframe['clock'].iloc[k+offset]
                    ack = dataframe['acknowledged'].iloc[k+offset]
                    while k + offset < df_len - 1 and dataframe['value'].iloc[k+offset] == dataframe['value'].iloc[k+offset+1]:
                        offset += 1
                        corrupted_objectids.add(objectid)
                    if k + offset == df_len - 1:
                        none_objectids_problems.append({'objectid': objectid, 'clock': clock, 'value': '1', 'acknowledged': ack, 'message': dataframe['message'].iloc[k+offset], 'priority': dataframe['priority'].iloc[k+offset], 'region': dataframe['region'].iloc[k+offset]})
                else:
                    while k + offset < df_len - 1 and dataframe['value'].iloc[k+offset] == dataframe['value'].iloc[k+offset+1]:
                        offset += 1
                        corrupted_objectids.add(objectid)
                    r_clock = dataframe['clock'].iloc[k+offset]
                    if clock is None:
                        none_objectids_problems.append({'objectid': objectid, 'clock': r_clock, 'value': '0', 'acknowledged': ack, 'message': dataframe['message'].iloc[k+offset], 'priority': dataframe['priority'].iloc[k+offset], 'region': dataframe['region'].iloc[k+offset]})
                    else:
                        make_row(objectid, objectids_problems, clock, r_clock, ack, dataframe['message'].iloc[k+offset], dataframe['priority'].iloc[k+offset], dataframe['region'].iloc[k+offset])

    problems_df = pd.DataFrame.from_dict(objectids_problems, orient='index')
    problems_df.reset_index(inplace=True)
    problems_df.rename(index=str, columns={'index': 'objectid'}, inplace=True)
    none_problems_df = pd.DataFrame(none_objectids_problems)
    return problems_df, none_problems_df, corrupted_objectids

def get_message(x):
    if type(x) is list:
        message = ''
        for message_dict in x:
            message += message_dict['message']
            if message_dict['message'][-1] != '.':
                message = message[:-1]
        return message

def get_region(xs):
    for x in xs:
        if 'region_' in x['name']:
            return x['name'].replace('region_', '')

engine = create_engine('mysql://reports:!Q2w3e4r5t6y@10.132.122.87/problems?charset=utf8')

for _ in range(100):
    try:
        with open('number_of_iteration.txt') as file:
            i = int(file.read()) + 1
            print(i)
    except:
        i=0
    start_time = 1556820000 - timedelta(days=i+1).total_seconds() # 3 may 00:00 local
    end_time = 1556820000 - timedelta(days=i).total_seconds()
    print('Подключаюсь к серверу...')
    z = ZabbixAPI()

    print('Собираю ивенты...')
    try:
        events = z.do_request('event.get', {
            'output': ['clock', 'r_clock', 'objectid', 'acknowledged', 'value', 'eventid'],
            'time_from': start_time,
            'time_till': end_time,
            'select_acknowledges': ['message']
            })
        df_events = pd.DataFrame(events['result'])
    except:
        for start, end  in zip(range(int(start_time), int(end_time)-3599, 3600), range(int(start_time)+3600, int(end_time)+1, 3600)):
            try:
                events = z.do_request('event.get', {
                    'output': ['clock', 'r_clock', 'objectid', 'acknowledged', 'value', 'eventid'],
                    'time_from': start,
                    'time_till': end,
                    'select_acknowledges': ['message']
                    })
            except:
                events = z.do_request('event.get', {
                    'output': ['clock', 'r_clock', 'objectid', 'acknowledged', 'value', 'eventid'],
                    'time_from': start,
                    'time_till': end,
                    })
            if start == start_time:
                df_events = pd.DataFrame(events['result'])
            else:
                events = pd.DataFrame(events['result'])
                df_events = pd.concat([df_events, events], sort=False)

    triggers = z.do_request('trigger.get', {
        'triggerids': df_events['objectid'].to_list(),
        'output': ['priority', 'groups'],
        'selectGroups': ['name'],
        })
    df_triggers = pd.DataFrame(triggers['result'])
    df_triggers['region'] = df_triggers['groups'].apply(get_region)
    df_events = pd.merge(df_events, df_triggers[['priority', 'triggerid', 'region']], left_on='objectid', right_on='triggerid')
    df_events['message'] = df_events['acknowledges'].apply(lambda x: get_message(x))
    df_events['objectid'] = df_events['objectid'].astype(int)
    df_events['clock'] = df_events['clock'].astype(int)
    #df_events['value'] = df_events['value'].astype(int)
    #df_events['acknowledged'] = df_events['acknowledged'].astype(float)
    try:
        none_problems_df = pd.read_csv('none_problems_df.csv')
        df_events = pd.concat([df_events, none_problems_df[none_problems_df['objectid'].isin(df_events['objectid'])]], sort=False)
    except:
        pass

    df_events['message'] = df_events['acknowledges'].apply(lambda x: get_message(x))
    events_by_objectid = df_events[['clock','objectid', 'acknowledged', 'value', 'eventid', 'message', 'priority', 'region']].groupby(['objectid'])
    events_by_objectid = pd.DataFrame(events_by_objectid)
    problems_df, none_problems_df, corrupted_objectids = create_problems_df(events_by_objectid)
    problems_df['accurate'] = True
    problems_df.loc[problems_df['objectid'].isin(corrupted_objectids), 'accurate'] = False
    problems_df.drop('acknowledged', axis=1, inplace=True)
    problems_df.to_sql('problems', con=engine, if_exists='append', index=False, dtype={'duration': types.Float, 'count': types.Float, 'clock': types.BIGINT, 'r_clock': types.BIGINT, 'accurate': types.Boolean})
    none_problems_df.to_csv('none_problems_df.csv', index=False)
    with open('number_of_iteration.txt', 'w') as file:
        file.write(str(i))
