from pyzabbix import ZabbixAPI
import pandas as pd
import io
from datetime import datetime
from sqlalchemy import create_engine, types
from rep import constants


class ZabbixReportv3:

    def __init__(self, zbx_web_url, zbx_user, zbx_pass, http_auth=False): #, host, user, password, db,ignored_duration=10):
        self.ZBX_WEB_URL = zbx_web_url
        self.ZBX_USER = zbx_user
        self.ZBX_PASS = zbx_pass
        self.z = ZabbixAPI(self.ZBX_WEB_URL)
        self.z.timeout = 35
        if http_auth:
            self.z.session.auth = (self.ZBX_USER, self.ZBX_PASS)
        self.z.login(self.ZBX_USER, self.ZBX_PASS)


    def get_events_df(self, start_time, end_time, duration, regions, priority_id):

        def get_unique_messages(x):
            x = x.split('|')
            x = set(x)
            x.discard('')
            return '. '.join(x)

        engine = create_engine()
        get_event_query = 'SELECT * FROM problems where clock>{} and r_clock<{} and priority>={}'.format(start_time, end_time, priority_id) + ' and region in (\"' + '\", \"'.join(regions) + '\")'
        events = engine.execute(get_event_query).fetchall()
        events_df = pd.DataFrame(events)
        events_df.columns = [i[0] for i in engine.execute("Describe problems").fetchall()]
        events_df = events_df.groupby('objectid').agg({'clock': 'min', 'r_clock': 'max', 'duration': 'sum', 'count': 'sum', 'accurate': 'prod', 'message': 'sum', 'region': 'first'}).reset_index()
        events_df['message'] = events_df['message'].apply(lambda x: get_unique_messages(x))
        return events_df[events_df['duration'] >= duration*60]


    def get_triggers_df(self, start_time, events_ids):

        triggers = self.z.do_request('trigger.get', {
            'output': ['priority', 'comments', 'description', 'groups', 'lastchange', 'value'],
            'selectGroups': ['name'],
            'expandDescription': 1,
            'sortfield': 'hostname',
            'sortorder': 'DESC',
            'triggerids': events_ids,
        })

        active_triggers = self.z.do_request('trigger.get', {
            'output': ['priority', 'comments', 'description', 'groups', 'lastchange', 'value'],
            'monitored': 1,
            'selectGroups': ['name'],
            'expandDescription': 1,
            'sortfield': 'hostname',
            'sortorder': 'DESC',
            'lastChangeTill': start_time,
            'filter': {
                'value': 1,
            }
        })

        def get_site(row):
            for i in range(len(row)):
                if 'ЦУС' in row[i]['name']:
                    return row[i]['name']
                if 'site_' in row[i]['name']:
                    return row[i]['name'].replace('site_', '')
            return ''

        triggers_df = pd.DataFrame(triggers['result'] + active_triggers['result'])
        #triggers_df['region'] = triggers_df.apply(lambda row: get_site_or_region(row, 'region_'), axis=1)
        triggers_df['site'] = triggers_df['groups'].apply(lambda row: get_site(row))
        triggers_df.drop('groups', axis=1, inplace=True)
        triggers_df['triggerid'] = triggers_df['triggerid'].astype(int)
        return triggers_df


    def write_to_excel(self, df):
        region_problems_quantity = df.groupby(['region'])['count'].sum()
        region_problems_quantity.sort_values(ascending=False, inplace=True)
        region_problems_quantity['Всего'] = region_problems_quantity.sum()
        output = io.BytesIO()
        headers = ['Филиал', 'РТС', 'Важность', 'Имя', 'Название проблемы',
                  'Начало возникновения инцидента',
                  'Время последнего изменения статуса',
                  'Кол-во повторений', 'Возраст проблемы', 'Комментарии']

        writer = pd.ExcelWriter(output, engine='xlsxwriter')
        df.to_excel(writer, sheet_name='Отчет', startrow=1, index=False, header=False)
        region_problems_quantity.reset_index().to_excel(writer, sheet_name='Анализ поврежд. по фил.',
                                                        header=['Филиал', 'Количество проблем'], index=False)
        workbook = writer.book
        header_format = workbook.add_format({'bold': True, 'text_wrap': True, 'valign': 'vcenter', 'align': 'center'})
        text_wrap_and_valign = workbook.add_format({'text_wrap': True, 'valign': 'vcenter', 'align': 'left'})
        worksheet1 = writer.sheets['Отчет']
        for index, header in enumerate(headers):
            worksheet1.write(0, index, header, header_format)
        worksheet1.set_default_row(35)
        #worksheet1.set_row(0, 40, text_wrap_and_valign)
        worksheet1.set_column(0, 8, 0, text_wrap_and_valign)
        for index, column in enumerate(df.columns):
            max_len = len(str(df[column].max())) * 1.25
            if max_len < len(column):
                max_len = len(column)
            elif max_len > 70:
                max_len -= max_len * 0.3
            worksheet1.set_column(index, index, max_len)
        sheetname = 'Анализ поврежд. по фил.'
        worksheet2 = writer.sheets[sheetname]
        chart = workbook.add_chart({'type': 'pie'})
        chart.add_series({'categories': "='" + sheetname + "'!A2:A" + str(region_problems_quantity.shape[0]),
                          'values': "='" + sheetname + "'!B2:B" + str(region_problems_quantity.shape[0])})
        worksheet2.insert_chart('D2', chart)
        worksheet2.set_column(0, 0, 30)
        worksheet2.set_column(1, 1, 20)
        writer.save()

        return output

    def make_report(self, start_time, end_time, ignored_duration, priority_id, regions):
        events_df = self.get_events_df(start_time, end_time, ignored_duration, regions, priority_id)
        triggers_df = self.get_triggers_df(start_time, list(events_df['objectid']))
        report_df = pd.merge(events_df, triggers_df, left_on='objectid', right_on='triggerid')
        report_df = report_df[['region', 'site', 'priority', 'hostname', 'comments', 'clock', 'r_clock', 'count','duration', 'message']].sort_values(by=['region', 'site'])
        report_df = report_df[report_df['priority'] >= priority_id]
        report_df['clock'] = pd.to_datetime(report_df['clock'], unit='s')
        report_df['r_clock'] = pd.to_datetime(report_df['r_clock'], unit='s')
        report_df['priority'] = report_df['priority'].apply(lambda x : constants.Constants.PRIORITIES[x])
        report_df['duration'] = report_df['duration'].apply(lambda x: '{} ч. {} мин. {} с.'.format(int(x // 3600), int((x - (x // 3600) * 3600) // 60),
                                                                             int(x - ((x // 3600) * 3600 + ((x - (x // 3600) * 3600) // 60) * 60))))
        return self.write_to_excel(report_df)